import faker from 'faker';
const logger = require('pino')({name: 'app:seeder'});
abstract class Seeder {
  
  public faker = faker          // FIX ME remove
  public logger: typeof logger;  // this will be used in sub classes
  public model: any;            // FIX ME remove
  public element = (): any => { return {} };  // FIX ME remove
  abstract developmentSeed(): any;  // FIX ME remove
  abstract productionSeed(): any;   // FIX ME remove
  
  constructor() {
    this.logger = logger.child({seed: this.constructor.name});
    this.logger.info('Initializing...');
  }

  async generate(n:any = 1, addSomenthig: any = false) {
    
    this.logger.info(`Generating`);

    let actual: number = 0;
    let result: Array<any> = [];

    if (typeof n === typeof 0) {

      this.logger.info(`${n} iterarions to do`);
      while (actual < n) {

        // this.logger.info(`${actual + 1}/${n} iterarion for ${this.constructor.name}`);

        let element = await this.element();

        if (addSomenthig !== false) {

          if (Array.isArray(element)) {
            element = element.map(e => {
              if (typeof addSomenthig === typeof (() => { })) {
                Object.assign(e, addSomenthig(e, actual));
              } else if (typeof addSomenthig === typeof {}) {
                Object.assign(e, addSomenthig);
              }
              return e;
            });
          } else if (typeof addSomenthig === typeof (() => { })) {
            Object.assign(element, addSomenthig(element, actual));
          } else if (typeof addSomenthig === typeof {}) {
            Object.assign(element, addSomenthig);
          }

        }

        if (Array.isArray(element)) {
          result = result.concat(element);
        } else {
          result.push(element);
        }

        actual++;

      }

    } else if (typeof n === typeof {}) {

      this.logger.info(`Generate static element`);

      let element = n;

      if (addSomenthig !== false) {

        if (typeof addSomenthig === typeof (() => { })) {
          Object.assign(element, addSomenthig(element));
        } else if (typeof addSomenthig === typeof {}) {
          Object.assign(element, addSomenthig);
        }

      }

      result.push(element);

    } else {
      this.logger.error(`Seems that somenthig is not going so well, bad object types`);
      return [];
    }

    this.logger.info(`Finished generating elements`);

    return result;

  }

  async get(element = false, options = { one: false }) {
    return await this._get(this.model, element, options);
  }
  async create(elements: any) {
    return await this._create(this.model, elements);
  }
  async save(elements: any) {
    return await this._save(elements);
  }
  async delete(elements: any) {
    return await this._remove(this.model, elements);
  }

  useModel(model: any) {
    this.logger.info('Using model %s', model.modelName);
    return {
      get: async (element:any = null, options = { one: false }) => {
        return await this._get(model, element, options);
      },
      create: async (elements: any) => {
        return await this._create(model, elements);
      },
      save: async (elements: any) => {
        return await this._save(elements);
      },
      delete: async (elements: any) => {
        return await this._remove(model, elements);
      }
    };
  }

  static map(elements: Array<any>, key: any) {
    return elements.map(el => el[key]);
  }

  static randomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  static randomBool() {
    return Math.random() >= 0.5;
  }

  static arrayUnique(a: any) {
    return [...new Set(a)];
  }

  static pickRandomFromArray(arr: Array<any>, params?: any) {

    let n = null;
    if (typeof params === typeof 0) {
      n = params;
    } else if (params && params.n === 0) {
      n = 0;
    } else {
      n = params && params.n ? params.n : 1;
    }

    if (!n) return [];

    if (params && params.excludeElement) {
      const randEl = (arr: Array<any>, excludeEl: any): any => {
        var randNumber = Math.floor(Math.random() * arr.length);
        if (arr[randNumber] === excludeEl) {
          return randEl(arr, excludeEl);
        } else {
          return arr[randNumber];
        }
      }

      let result = [];
      for (let i = 0; i < n; i++) {
        result.push(randEl(arr, params.excludeElement));
      }

      return Seeder.arrayUnique(result);

    }

    var result = new Array(n);
    var len = arr.length;
    var taken = new Array(len);
    if (n > len) {
      n = len;
    }

    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;

  }


  private async _get(model: any, element: any = false, options:any = { one: false }) {

    let method = 'find';
    if (options.one) {
      method += 'One';
    }

    if (!element) {
      return await model[method]({});
    } else if (Array.isArray(element)) {
      return await model[method](element[0]);
    } else {
      return await model[method](element);
    }

  }

  private async _create(model: any,elements: any) {

    if (!Array.isArray(elements)) {
      elements = [elements];
    }

    await model.insertMany(elements);
    return elements;

  }

  private async _save(elements: any) {

    if (!Array.isArray(elements)) {
      elements = [elements];
    }
    const promises = [];
    for (let element of elements) {
      promises.push(new Promise((resolve, reject) => {
        element.save((err: Error) => {
          resolve(true);
        });
      }))
    }
    return await Promise.all(promises);

  }

  private async _remove(model: any, elements: any) {
    if (!Array.isArray(elements)) {
      elements  = [elements];
    }
    elements = elements.map((el:any) => el._id);
    return await model.deleteMany({
      _id: {
        $in: elements
      }
    });
  }

}

export {
  Seeder
}
