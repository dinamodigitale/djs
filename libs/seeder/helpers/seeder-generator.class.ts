import fs from 'fs';
import mongoose from 'mongoose';
import { URL } from 'url';

const beautify = require('js-beautify').js;

class SeederGenerator {
    
  content: string;
  defaultSchema: { foo: null; bar: null; };
  path: String = '';
  
  constructor(path:any) {

    const fileUrl = new URL(`file://${__dirname}/../basic-seed.txt`);
    this.path = path;

    this.content = fs.readFileSync(fileUrl).toString();
    this.defaultSchema = {
      foo: null,
      bar: null
    };

  }

  generate(mainClassName: String, model: String|null = null) {

    if(typeof mainClassName !== typeof "string") {
      throw new Error('must specify a class name');
    }
    this.content = this.content.replace('${mainClassName}', SeederGenerator.camelize(mainClassName));

    if (model) {

      this.content = this.content.replace('${model}', SeederGenerator.capitalize(model));
      
      let basicSchema: any;
      try {
        basicSchema = mongoose.model(SeederGenerator.capitalize(model)).schema.obj;
        Object.keys(basicSchema).forEach(key => {
          basicSchema[key] = null;
        });
      } catch(e) {
        basicSchema = this.defaultSchema;
      }

      this.content = this.content.replace('${element}', JSON.stringify(basicSchema, null, 2));

    } else {

      this.content = this.content.replace('${model}', 'FooBar').replace('${element}', JSON.stringify(this.defaultSchema, null, 2));

    }

    const fileName = `${+new Date()}_${SeederGenerator.snakize(mainClassName)}.seed.ts`;
    
    this.content = beautify(this.content, {
      indent_size: 2,
      space_in_empty_paren: true
    });

    fs.writeFileSync(`${this.path}/${fileName}`, this.content);

    return this.content;

  }

  static capitalize(s: String) {
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  static camelize(string: any) {

    const chars = ['_', '.', '-', ':', ' '];
    string = [string];
    chars.forEach(c => {
      string = [].concat.apply([], string.map((s: String) => s.trim().split(c)));
    });
    
    string = string.map((s: String) => SeederGenerator.capitalize(s));
    
    return string.join('');

  }

  static snakize(string: any) {

    const chars = ['_', '.', '-', ':', ' '];
    string = [string];
    chars.forEach(c => {
      string = [].concat.apply([], string.map((s:string) => s.trim().split(c)));
    });
    
    return string.join('_');

  }
  
}
export  {
  SeederGenerator
};
