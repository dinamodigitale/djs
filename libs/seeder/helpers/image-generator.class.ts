import gm from 'gm';
import fs from 'fs';
import faker from 'faker';

const download = require('image-downloader')
var mime = require('mime-to-extensions')

class ImageGenerator {

  baseUrl: any;
  destination: any;
  interpolators: any;
  createLocal: any;
  palette: any;

  constructor(interpolators: any, params: any) {

    this.baseUrl = params && params.baseUrl ? params.baseUrl : 'https://picsum.photos/{{width}}/{{height}}/?random';
    this.destination = params && params.destination ? params.destination : '/tmp';
    this.interpolators = {};

    this.createLocal = params && params.createLocal;

    this.palette = params && params.palette ? params.palette : [
      '#D9E5D6',
      '#75B9BE',
      '#EDDEA4',
      '#F7A072',
      '#FF9B42'
    ];

    if (this.createLocal) {
      this.interpolators = interpolators;
    } else {
      Object.keys(interpolators).forEach(k => {
        this.interpolators[k] = {
          default: interpolators[k],
          cb: (string: String, value: any) => string.replace(`{{${k}}}`, value)
        };
      });
    }

  }

  randomImageName(mimeType?: String) {
    const extension = mimeType ? mime.extension(mimeType) : 'jpg';
    return `img_${Math.floor(Math.random() * 1000000)}.${extension}`;
  }

  get randomColor() {
    return this.palette[Math.floor(Math.random() * this.palette.length)];
  }
  get randomText() {
    return faker.random.words(3);
  }

  pickRandom(interpolators: any = {}) {

    if (!fs.existsSync(this.destination)) {
      fs.mkdirSync(this.destination);
    }

    if (this.createLocal) {

      let width = this.interpolators.width || 1920;
      let height = this.interpolators.height || 1080;
      let fontSize = this.interpolators.fontSize || width * .04 || 32;

      let fontX = width / 100 * 2;
      let fontY = (height - fontSize / 2) / 100 * 50;

      const filename = this.randomImageName();
      const path = `${this.destination}/${filename}`;

      if (interpolators.width) {
        width = interpolators.width;
      }
      if (interpolators.height) {
        height = interpolators.height;
      }
      if (interpolators.fontSize) {
        fontSize = interpolators.fontSize;
      }

      return new Promise((resolve, reject) => {

        gm(width, height, this.randomColor)
          .region(width * .95, height * .95, width * .025, height * .025)
          .gravity('SouthEast')
          .fontSize(fontSize * .8)
          .drawText(0, 0, `${width} x ${height}`)
          .write(path, err => {
            gm(path)
              .region(width, height, width, height)
              .gravity('Center')
              .fontSize(fontSize)
              .drawText(0, 0, this.randomText).write(path, () => {
                if (err) {
                  reject(err);
                } else {
                  resolve({
                    path,
                    filename
                  });
                }
              })
          });

      });

    } else {
      return download.image({
        url: this.getParsedUri(interpolators),
        dest: `${this.destination}/${this.randomImageName()}`
      }).then((res: any) => {
        return {
          filename: res.filename.split('/')[res.filename.split('/').length - 1],
          path: res.filename.replace('./', '/')
        }
      });
    }

  }

  getParsedUri(interpolators: any = {}) {

    let requestUrl = this.baseUrl;
    Object.keys(this.interpolators).forEach(k => {
      const useValue = interpolators[k] ? interpolators[k] : this.interpolators[k].default;
      requestUrl = this.interpolators[k].cb(requestUrl, useValue);
    });
    return requestUrl;

  }

}

export default ImageGenerator;