import jsonfile from 'jsonfile';
//FIXME Remove dep of json file and use require() instead

const logger = require('pino')({name: 'app:configuration'});

class Configuration {

  folder: String = '';

  constructor(configurationsPath?: String) {
    if(configurationsPath) {
      this.setPath(configurationsPath)
    }
  }

  get(name: String) {
    logger.warn("Depricaiton warning, instance method is depricaed and will be remove, please use Configuration.get(name: String) instead")
    const filename = `${this.folder}/${name}.config.json`;
    process.env.DEBUG && logger.info(`Reading file ${filename}`);
    try {
      return jsonfile.readFileSync(filename);
    } catch (e) {
      logger.warn(`** WARNING **, Missing config file ${filename}`);
    }
    return {};
  }

  setPath(configurationsPath: String) {
    this.folder = configurationsPath;
  }

  static get(name: String): any {
    const filename = `${name}.config.json`;
    process.env.DEBUG && logger.info(`Reading configuration file ${filename}`);
    try {
      const baseContent = require(`@configurations/${name}.config.json`);
      try {
        const envContent = require(`@configurations/${name}.${process.env.NODE_ENV}.config.json`);
        process.env.DEBUG && logger.info(`Merging config file base on the current env ${name}.${process.env.NODE_ENV}.config.json`)
        return {...baseContent,...envContent};
      } catch (e) {
        process.env.DEBUG && logger.info(`No env config file found, ${name}.${process.env.NODE_ENV}.config.json`)
      }
      return baseContent
    } catch(e) {
      logger.warn(`** WARNING **, Missing config file ${filename}`, e.message);
      
    }
    return {}
  }

}

export default Configuration;