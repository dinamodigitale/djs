import mongoose from 'mongoose';
import { pagination } from './plugins';

abstract class DinamoAppDbmsInterface {
  public abstract connected: boolean;
  public abstract connect(): any;
  public abstract disconnect(): any;

}

const logger = require('pino')({name: 'app:mongoose-loader'});

class MongooseLoader extends DinamoAppDbmsInterface {
  public connected = false;
  private connectionOptions: any;
  

  constructor(connectionOptions?: any) {
    super();
    this.connectionOptions = connectionOptions;
    process.env.DEBUG && logger.info("Loading pagination plugin");
    mongoose.plugin(pagination);
    mongoose.set('debug', process.env.DJS_MONGOOSE_DEBUG != 'true' ? false : true);
  }



  async disconnect() {
    logger.warn("Disconnecting")
    return await mongoose.disconnect();
  }

  connect(onOpen?: Function, onConnect?: Function, onDisconnect?: Function) {

    const options = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,

      // reconnectTries: Number.MAX_VALUE,
      // reconnectInterval: 1000,
      poolSize: 10,
      bufferMaxEntries: 0,
      connectTimeoutMS: 1000,
      socketTimeoutMS: 60000,
    };

    process.env.DEBUG && logger.info("Trying to connection to DB");
    
    if(!this.connectionPath()) {
      logger.error(`Bad mongodb config, please check mongoose-loader.config.json or set the env variable DJS_MONGODB_CONNECTION`);
    }
    
    mongoose.connect(this.connectionPath(), options).catch(e => logger.error) ;

    mongoose.connection.on('error', (err: mongoose.Error) => {
      // it seems to be a bug in the internal mongo driver
      // in some cases it won't trace the connection erro
      // this happened when the network between the driver 
      // and the server goes down!
      if(err.name === "MongoNetworkError" || err.message.includes("ENOTFOUND") || err.message.includes("ECONNREFUSED") ) {
        setTimeout(() => {
          logger.info("Retrying connection to database")
          mongoose.connect(this.connectionPath(), options).catch(e => logger.error)
        },5000)
      }
      logger.error(`DB connection error %o`, err);
    });
    
    mongoose.connection.on('open', async () => {
      process.env.DEBUG && logger.info("DB connection open");
      onOpen && await onOpen();
    });
    
    mongoose.connection.on('connected', async () => {
      process.env.DEBUG && logger.info("DB connected");
      onConnect && await onConnect();
    });
    
    mongoose.connection.on('disconnected', async () => {
      process.env.DEBUG && logger.info("DB disconected");
      onDisconnect && await onDisconnect();
    });

  }

  private connectionPath() {
    return this.connectionOptions.connection || process.env.DJS_MONGODB_CONNECTION;
  }
}

export { mongoose };
export default MongooseLoader;