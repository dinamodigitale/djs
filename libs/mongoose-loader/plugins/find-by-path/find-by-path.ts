import { Schema } from 'mongoose';

export interface OptionsInterface {
  path: string,
  locale: string
}

const logger = require('pino')({name: 'app:djs:mongoose:plugins', plugin: 'find-by-path'});

const findByPath = (schema: Schema) => {
  schema.query.getPage = async function (opts: OptionsInterface) {
    const path = opts.path;
    const locale = opts.locale;

    const splittedPath: string[] = path.split('/');

    if (splittedPath && splittedPath.length) {
      for (const slug of splittedPath) {
        const queryNoMultiLang = {
          slug: slug
        }
        const queryMultiLang = {
          [`slug.${locale}` || 'it']: slug
        }
        
        let page = await this.where(
          opts && opts.locale ? queryMultiLang : queryNoMultiLang
        )
        if (!page) {
          logger.warn(`- slug "${slug}" NON corrisponde a nessuna pagina -`);
          return null;
        }
      }
    }

    if (opts && opts.locale) {
      return this.where({
        [`slug.${locale}` || 'it']: splittedPath[splittedPath.length - 1],
      });
    } else {
      return this.where({
        slug: splittedPath[splittedPath.length - 1],
      })
    }
  };
};

export { findByPath };
