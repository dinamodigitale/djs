export default interface NestedSetInterface<T> {
  children: Array<T>;
  parent: T;
}
