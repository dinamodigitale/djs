import { Schema, model, Model } from "mongoose";
import { mongoose } from "../../mongoose-loader";

const logger = require('pino')({name: 'app:djs:mongoose:plugins', plugin: 'nested-set'});

const ValidationError = mongoose.Error.ValidationError;
const ValidatorError = mongoose.Error.ValidatorError;
export const NESTED_SET_ROOT_ID = '000000000000000000000000';

interface MinimalElementInterface {
	_id: string;
	children: Array<string>;
}
interface ElementInterface {
	_id: string;
	children: Array<ElementInterface>;
}

export interface ModelToTreeOptionsInterface {
	depth?: number;
	startingFrom?: string;
  filter?: any,
  select?: string
}
export const basicModelToTreeOptions: ModelToTreeOptionsInterface = {
	depth: 0
};

const _reorderElementChildren = (element: ElementInterface, stone: Array<MinimalElementInterface>) => {

	if (element && element.children.length) {

		const stoneParent = stone.find((sc: any) => {
			return sc._id.toString() === element._id.toString();
		});

		if (stoneParent) {

			const orderedChildren = stoneParent.children;

			element.children.sort((a, b) => {
				return orderedChildren.findIndex(c => {
					return c.toString() === a._id.toString()
				}) - orderedChildren.findIndex(c => {
					return c.toString() === b._id.toString()
				});
			});

		}

	}
	return element;

};

export const listToTree = (list: Array<any>, stone: Array<any>, modelToTreeOptions?: ModelToTreeOptionsInterface) => {
	modelToTreeOptions = modelToTreeOptions ? modelToTreeOptions : basicModelToTreeOptions;
	// First level mapping
	modelToTreeOptions && modelToTreeOptions.filter && (list = list.filter(modelToTreeOptions.filter))
	let tree = list.map(doc => {
		// Second level mapping
		doc.children = (doc.children || []).map((docChild: any) => {
			// third level assigning
			docChild.children = (docChild.children || []).map((d: any) => {
				return doc.children.find((ddc: any) => {
					return ddc._id.toString() === d.toString()
				});
			});
			return _reorderElementChildren(docChild, stone);
		}).filter((d: any) => d.__depth === 0);
		return _reorderElementChildren(doc, stone);
	});
	return tree;
}

const save = function (obj: any): Promise<any> {
	return new Promise((resolve, reject) => {
		obj.save((err: Error) => {
			if (err) reject(err);
			else resolve(obj);
		})
	});
}

const nestedSet = (schema: Schema, opt?: any) => {

	schema.add({
		children: [{
			type: Schema.Types.ObjectId,
		}],
		parent: {
			type: Schema.Types.ObjectId,
			default: NESTED_SET_ROOT_ID,
		}
	});

	schema.statics.tree = async function (modelToTreeOptions?: ModelToTreeOptionsInterface) {

		const id = modelToTreeOptions && modelToTreeOptions.startingFrom ? modelToTreeOptions.startingFrom : NESTED_SET_ROOT_ID;

		const $match = {
			_id: mongoose.Types.ObjectId(id)
		};
		const $unwind = '$children';
		const $lookup = {
			from: this.collection.collectionName,
			localField: "children",
			foreignField: "_id",
			as: "children"
		};
		const $replaceRoot = { newRoot: '$children' };
		
		const $graphLookup = {
			from: this.collection.collectionName,
			startWith: "$children",
			connectFromField: "children",
			connectToField: "_id",
			depthField: '__depth',
			as: "children"
		};

		const pipeline: any = [
			{ $match },
			{ $unwind },
			{ $lookup },
			{ $unwind },
			{ $replaceRoot },
			{ $graphLookup }
    ];
    
    if(modelToTreeOptions && modelToTreeOptions.select) {
      let $project: any = {};
      
      modelToTreeOptions.select.split(/\s+/).forEach(p =>$project[p] = 1);
      pipeline.push({$project});
    }
		const p = await Promise.all([
			await this.aggregate(pipeline),
			this.find({
				"children.1": {
					$exists: true
				}
			}).select('children')
		]);

		const pages = p[0];
		const stone = p[1];

		return listToTree(pages, stone, modelToTreeOptions);

	}

	schema.statics.move = async function (source: string, where: string, target: string) {

		logger.info("Moving object %s %s %s", source, where, target);

		if (!(['in', 'before', 'after'].indexOf(where) >= 0)) {
			throw new Error('You can move in, before and after a target');
		}

		if ('in' === where) {
			await this.findOneAndUpdate({ _id: source }, { parent: target }, { new: true, runValidators: true, useFindAndModify: false });
			//@ts-ignore
			return this.tree();
		} else {

			let targetObj = await this.findOne({ _id: target });
			let sourceObj = await this.findOneAndUpdate({ _id: source }, { parent: targetObj.parent }, { new: true, runValidators: true, useFindAndModify: false });
			let parentObj = await this.findOne({ _id: targetObj.parent });
			let sourceIndex = parentObj.children.indexOf(source);
			parentObj.children.splice(sourceIndex, 1);
			let targetIndex = parentObj.children.indexOf(targetObj._id);
			let plus = +('after' === where)
			parentObj.children.splice(targetIndex + plus, 0, sourceObj._id);
			await save(parentObj);
		}
		//@ts-ignore
		return this.tree();

	}

	schema.pre<any>('remove', async function (this: any, next: Function) {
		const self = this;
		if (this.children.length > 0) {
			const error = new ValidationError(self);
			error.errors.parent = new ValidatorError('Can\'t delete page when it has children');
			return next(error);
		}
		return next();
	});

	schema.pre<any>('findOneAndRemove', async function (this: any, next: Function) {
		const self = this;
		const oldSelf = await this.model.findOne(self.getQuery()).populate('parent');

		if (null === oldSelf) {
			return next();
		}

		if (oldSelf.children.length > 0) {
			const error = new ValidationError(oldSelf);
			error.errors.parent = new ValidatorError('Can\'t delete page when it has children');
			return next(error);
		}
		oldSelf.parent.children.splice(oldSelf.parent.children.map((s: any) => s.toString()).indexOf(oldSelf._id.toString()), 1)
		//await oldSelf.parent.save((e: Error) => { if(e) throw new Error(e.message) })
		await save(oldSelf.parent);
		return next();
	});

	schema.pre<any>('save', async function (this: any, next: Function) {
		const self = this;
		if (!self.isNew) {
			return next();
		}

		// New root element
		if (!self.parent) {
			return next();
		}
		// New and has a parent
		else if (self.parent) {

			if (self._id.toString() === NESTED_SET_ROOT_ID) {
				self.parent = null;
				return next();
			}

			const parent = await this.constructor.findOne({ _id: self.parent });
			if (!parent) {
				self.invalidate('parent', 'Parent not found!');
				const error = new ValidationError(self);
				error.errors.parent = new ValidatorError('parent not found');
				return next(error);
			}
			parent.children.push(self._id);
			//await parent.save((e: Error) => { if(e) throw new Error(e.message) });
			await save(parent);
			return next();
		}
	});

	schema.pre<any>('findOneAndUpdate', async function (this: any, next: Function) {
		const self = this;
		const oldSelf = await this.model.findOne(self.getQuery());
		const newSelf = self.getUpdate();

		if (oldSelf && newSelf.parent) {
			const newParent = await this.model.findOne({ _id: newSelf.parent });

			if (!newParent) {
				// @ts-ignore
				const error = new ValidationError();
				error.errors.parent = new ValidatorError('Cound not find any document with the specified _id');
				return next(error);
			}

			if (newParent._id.toString() === oldSelf._id.toString()) {
				// @ts-ignore
				const error = new ValidationError();
				error.errors.parent = new ValidatorError('Document and parent are the same');
				return next(error);
			}

			let oldParent = await this.model.findOne({ _id: oldSelf.parent });
			if (!oldParent) {
				// Old parent disappeared
				oldParent = new this.model();
			}
			if (oldParent._id.toString() !== newParent._id.toString()) {
				oldParent.children = oldParent.children.filter((c: any) => {
					return c && c.toString() != oldSelf._id.toString()
				});
				newParent.children.push(oldSelf._id);
				// moving to the same path?
				const family = await this.model.aggregate([
					{ $match: { _id: oldSelf._id } },
					{ $graphLookup: { from: this.mongooseCollection.name, startWith: "$children", connectFromField: "children", connectToField: "_id", as: "family" } },
					{ $project: { family: { _id: 1 } } },
					{ $match: { family: { _id: newParent._id } } }
				]);

				if (family.length > 0) {
					// @ts-ignore
					const error = new ValidationError();
					error.errors.parent = new ValidatorError('Can\'t move the document to one of its children');
					return next(error);
				}
				await Promise.all([save(oldParent), save(newParent)]);
				oldSelf.children = oldSelf.children.filter((c: any) => c._id.toString() != newParent._id.toString());
				await save(oldSelf);


				return next();
			}
		}
		return next();
	});

};

export { nestedSet };