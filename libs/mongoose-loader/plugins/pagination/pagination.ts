import { Schema } from "mongoose";
import { Request, Response } from 'express';
const logger = require('pino')({name: 'app:djs:mongoose:plugins', plugin: 'pagination'});

export const PAGINATION_DEFAULT_LIMIT = 25;
export const PAGINATION_DEFAULT_PAGE = 1;

const pagination = (schema: Schema<any>, opt?: any) => {
 
  schema.query.paginate = async function (req: Request, res: Response) {
    logger.warn("Deprecation warning, method paginate will be removed, use pagination() instead", __filename);
    let page:number = req.query.page ? +req.query.page || 1 : 1;
    let limit:number = req.query.limit ? +req.query.limit || PAGINATION_DEFAULT_LIMIT : PAGINATION_DEFAULT_LIMIT;
    page = Math.max(1, page);
    limit = Math.max(1, limit);
    !page && (page = PAGINATION_DEFAULT_PAGE);
    !limit && (limit = PAGINATION_DEFAULT_LIMIT);

    const totalRecords = await this.model.find(this._conditions).select('_id').limit(10000).countDocuments();
    let headers = {
      'X-Pagination-Total-Records': totalRecords,
      'X-Pagination-Page': page,
      'X-Pagination-Limit': limit,
      'X-Pagination-Total-Pages': Math.ceil(totalRecords / limit),
      'X-Pagination-Has-Next-Page': Math.ceil(totalRecords / limit) > page,
      'X-Pagination-Has-Prev-Page': page > 1
    };
    process.env.DEBUG && logger.info("Paginating %o query %o, results: %o", this.model.modelName, req.query, headers);
    res.set(headers);
    return await this.skip((page - 1) * limit).limit(limit);
  };

  schema.query.pagination = async function (limit?: number, page?: number, lean?: boolean) {

    page = Math.max(0, page || 0);
    limit = Math.max(1, limit || PAGINATION_DEFAULT_LIMIT);
    page === undefined || page === null || page < 0  && (page = 0);
    !limit && (limit = PAGINATION_DEFAULT_LIMIT);
    process.env.DEBUG && logger.info("Pagination data: page: %o, limit: %o",page,limit)
    let start = +new Date();
    const totalRecords = await this.model.find(this._conditions).select('_id').limit(process.env.PAGINATION_MAX_LIMIT || 10000).countDocuments();
    let data: any;
    if(lean) {
      data = await this.skip(page * limit).limit(limit).lean();
    } else {
      data = await this.skip(page * limit).limit(limit);
    }
    // if(lean) {
    // }
    let pagination = {
      limit,
      page,
      totalRecords,
      totalPages: Math.ceil(totalRecords / limit),
      hasNextPage: Math.ceil(totalRecords / limit) > page,
      hasPrevPage: page != 0
    };
    return {pagination, data, };
  };
}

export { pagination }