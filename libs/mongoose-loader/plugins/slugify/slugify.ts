import { Schema, Document } from "mongoose";
import { I18n } from "../../../i18n";

export interface SlugifyOptionsMongoosePlugin {
  from?: string | Function,
  fallback?: boolean,
}

const slugify = (schema: Schema, opts: SlugifyOptionsMongoosePlugin) => {
  opts = {
    from: 'slug',
    fallback: true,
    ...opts
  }
  schema.pre('validate', hookUpdateSlug)
  // schema.pre('save', hookUpdateSlug)
  // schema.pre('update', hookUpdateSlug)

  async function hookUpdateSlug(this: Document, next: Function) {
    const slug = schema.path('slug')
    const doc: any = this;
    let   initSlug: any;

    if (!slug) {
      throw new Error('No slug found in schema')
    }

    if (opts.from === undefined) {
      throw new Error("Missing from in options");
    }

    if(typeof opts.from !== 'function' && opts.from && undefined === doc[opts.from]) {
      throw new Error(`Bad from option, the model does not contain ${opts.from}`)
    }    

    if (typeof opts.from === 'function') {
      initSlug = opts.from(doc) || doc._id;
    }
    else if (typeof doc[opts.from] === 'string') {
      initSlug = doc[opts.from] || doc._id;
    }
    else if (typeof doc[opts.from] === 'object') {
      initSlug = {}
      for (let locale of Object.keys(doc[opts.from]).filter(l => l !== '_i18n')) {
        initSlug[locale] = doc[opts.from][locale]
      }
    }

    if (typeof initSlug === 'string') {
      doc.slug = doc.slug || I18n.slugify(initSlug);
    }
    else if (typeof initSlug === 'string' && typeof doc.slug === 'object') {
      for (let locale of Object.keys(doc.slug).filter(l => l !== '_i18n')) {
        doc.slug[locale] = I18n.slugify(doc.slug[locale] || initSlug);
      }
    }
    else if (typeof initSlug === 'object') {
      for (let locale of Object.keys(initSlug).filter(l => l !== '_i18n')) {
        doc.slug[locale] = I18n.slugify(doc.slug[locale] || initSlug[locale]) || doc._id
      }
    }

    this.markModified && this.markModified('slug');
    next()
  }
}



export { slugify }