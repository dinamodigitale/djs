
import MongooseLoader from './mongoose-loader';
import { mongoose } from './mongoose-loader';

export * from './plugins';

export {
  MongooseLoader,
  mongoose,       // To be depricated in future
};