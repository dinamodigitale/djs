export default interface DFileInterface<T> {
  path: string,
  mimetype: T,
  originalname: string,
  size: number,
  attributes?: any
}
