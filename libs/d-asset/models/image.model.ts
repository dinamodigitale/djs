import DFileInterface from './file.model';

export default interface DImageInterface {
  [x: string]: any;
  file: DFileInterface<'image/jpg' | 'image/png'>
}
