import DFileInterface from './file.model';

export default interface DPDFInterface {
  [x: string]: any;
  file: DFileInterface<'application/pdf'>
}
