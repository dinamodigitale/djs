import DImage from './d-image.class';
import MulterAsset from './multer-asset.class';

export {
  DImage,
  MulterAsset
};
