import DAsset from './d-asset.class';
import gm, { State, ResizeOption } from 'gm';
import fs from 'fs';

const sizeOf = require('image-size');
const fsExtra = require('fs-extra');

interface DImageRatio {
  width: number | null;
  height: number | null;
  cropped: boolean;
}

export { DImageRatio };

const basicQualities: ReadonlyArray<[string, number]> = [
  ['very-low', 20],
  ['low', 33],
  ['medium', 66],
  ['high', 80],
  ['very-high', 100]
];

const basicRatios: ReadonlyArray<[string, DImageRatio]> = [
  ['1920x1080', {
    cropped: false,
    width: 1920,
    height: 1080
  }],
  ['1920x1080c', {
    cropped: true,
    width: 1920,
    height: 1080
  }],
  ['1920x', {
    cropped: false,
    width: 1920,
    height: null
  }],
  ['x1080', {
    cropped: false,
    width: null,
    height: 1080
  }]
];

export default class DImage extends DAsset {

  gm = gm;
  imagePath: string;
  image: State;

  constructor(
    imagePath: string
  ) {

    super(imagePath);

    this.imagePath = imagePath;
    this.image = this.gm(this.imagePath);

  }

  static availableQualities(qualities?: ReadonlyArray<[string, number]>): Map<string, number> {
    return new Map(qualities || basicQualities);
  }
  static availableRatios(ratios?: ReadonlyArray<[string, DImageRatio]>): Map<string, DImageRatio> {
    return new Map(ratios || basicRatios);
  }

  resize(width: number, height: number, options: ResizeOption = '>') {
    this.image = this.image.resize(width, height, options);
    return this;
  }

  crop(width: number, height: number) {
    this.image = this.image.crop(width, height);
    return this;
  }

  gravity(direction: string) {
    this.image = this.image.gravity(direction);
    return this;
  }

  quality(level: number) {
    this.image = this.image.quality(level);
    return this;
  }

  compress(type: string) {
    this.image = this.image.compress(type);
    return this;
  }

  imageSize() {
    return sizeOf(this.imagePath);
  }

  processImage(ratio: DImageRatio, options?: ResizeOption) {
    
    if (!ratio.cropped) {
      if (ratio.width && ratio.height) {
        this.image.resize(
          //@ts-ignore
          ratio.width < this.imageSize().width ? ratio.width  : null, 
          ratio.height < this.imageSize().height ? ratio.height : null, 
          options);
      } else if (ratio.width && !ratio.height) {
        // @ts-ignore
        this.image.resize(ratio.width < this.imageSize().width ? ratio.width  : null, null, options);
      } else if (!ratio.width && ratio.height) {
        // @ts-ignore
        this.image.resize(null, ratio.height < this.imageSize().height ? ratio.height : null, options);
      }
    } else {
      if (ratio.width && ratio.height) {
        let orientation = this.imageSize().width > this.imageSize().height ? 'h' : 'v'
        if (orientation === 'h') {
          // @ts-ignore
          this.image.resize(null, ratio.height, options).gravity('Center').crop(ratio.width, ratio.height);
        } else {
          // @ts-ignore
          this.image.resize(ratio.width, null, options).gravity('Center').crop(ratio.width, ratio.height);
        }
      } else if (ratio.width && !ratio.height) {
        // @ts-ignore
        this.image.resize(ratio.width, null, options).gravity('Center').crop(ratio.width, ratio.height);
      } else if (!ratio.width && ratio.height) {
        // @ts-ignore
        this.image.resize(null, ratio.height, options).gravity('Center').crop(ratio.width, ratio.height);
      }
    }

    return this;

  }

  write(newImagePath: string) {

    const filename = newImagePath.split('/')[newImagePath.split('/').length - 1];
    const newDir = newImagePath.replace(`/${filename}`, '');

    fsExtra.ensureDirSync(newDir);

    return new Promise((resolve, reject) => {
      this.image.write(newImagePath, (err) => {
        if (!err) {
          resolve(this.image);
        } else {
          reject(err);
        }
      });
    });
  }

  static elaborateRatioString(ratio: string, availableRatios?: ReadonlyArray<[string, DImageRatio]>): DImageRatio | boolean {
    if (!ratio) {
      ratio = '';
    }
    return DImage.availableRatios(availableRatios).get(ratio) || false;
  }

  static elaborateQualityString(quality?: string, availableQualities?: ReadonlyArray<[string, number]>): number | boolean {
    if (!quality) {
      quality = '';
    }
    return DImage.availableQualities(availableQualities).get(quality) || false;
  }

  static assetExists(path: string) {
    return fs.existsSync(path);
  }

}
