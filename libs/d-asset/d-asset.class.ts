const fs = require('fs-extra');
const readChunk = require('read-chunk');
import fileType from 'file-type';

class DAsset {

  assetPath: string;
  assetBuffer: Buffer;
  assetName: string;
  uploadsBaseDir: String = 'uploads';

  constructor(
    assetPath: string
  ) {
    this.assetPath = assetPath;
    this.assetBuffer = Buffer.from(this.assetPath);
    this.assetName = this.assetPath.split('/')[this.assetPath.split('/').length - 1];
  }

  get mimeType() {
    return fileType(this.assetBuffer);
  }

  static getMimeType(asset: Buffer) {
    return (fileType(Buffer.from(asset)) || { mime: '' }).mime;
  }

  static getMimeTypeByPath(assetPath: string) {
    const buffer = readChunk.sync(assetPath, 0, fileType.minimumBytes);
    return (fileType(buffer) || { mime: null }).mime;
  }

  static size(assetBuffer: Buffer) {
    const stats = fs.lstatSync(assetBuffer);
    return stats.size;
  }

  fileNamePathComposer(filename: Boolean | String = false) {

    var now = new Date();
    return [
      this.uploadsBaseDir,
      now.getFullYear(),
      now.getMonth() + 1,
      now.getDate(),
      typeof filename === typeof 'string' ? (filename as String).replace(/[^a-z0-9\.]/gi, '_').toLowerCase() : false
    ].filter(f => f !== false && f !== true).join('/');

  }

  create() {

    const mimetype = DAsset.getMimeTypeByPath(this.assetPath);
    const size = DAsset.size(this.assetBuffer);

    const newDir = this.fileNamePathComposer(false);
    fs.ensureDirSync(newDir);

    const newFileName = `${+new Date()}-${this.assetName}`;
    const newPath = this.fileNamePathComposer(newFileName);
    fs.renameSync(this.assetPath, newPath);

    return {
      path: newPath,
      mimetype,
      originalname: this.assetName,
      filename: newFileName,
      size
    };

  }

}

export default DAsset;
