const fs = require("fs-extra");
const imageSize = require("image-size");

export default class MulterAsset {
	asset: Express.Multer.File;
	entity: Boolean | String = false;
	uploadsBaseDir: String = "uploads";

	constructor(
		asset: Express.Multer.File,
		entity?: String,
		uploadsBaseDir?: String
	) {
		this.asset = asset;
		this.entity = entity ? entity : this.entity;
		this.uploadsBaseDir = uploadsBaseDir
			? uploadsBaseDir.trim().toLowerCase()
			: this.uploadsBaseDir;
		if (this.uploadsBaseDir[0] === "/") {
			this.uploadsBaseDir = this.uploadsBaseDir.substring(1);
		}
	}

	fileNamePathComposer(filename: Boolean | String = false) {
		var now = new Date();
		return [
			this.uploadsBaseDir,
			now.getFullYear(),
			now.getMonth() + 1,
			now.getDate(),
			typeof filename === typeof "string"
				? (filename as String).replace(/[^a-z0-9\.]/gi, "_").toLowerCase()
				: false
		]
			.filter(f => f !== false && f !== true)
			.join("/");
	}

	create() {
		const newDir = this.fileNamePathComposer(false);
		fs.ensureDirSync(newDir);
		const formatted = this.asset.originalname.split('.')
		const ext = formatted.pop()

		const newFileName = `${formatted.join('-')}-${+new Date()}.${ext}`;
		const newPath = this.fileNamePathComposer(newFileName);
		fs.renameSync(this.asset.path, newPath);

		let attributes = {};
		try {
			attributes = imageSize(newPath);
		} catch (e) {}

		return {
			path: newPath,
			mimetype: this.asset.mimetype,
			originalname: this.asset.originalname,
			filename: newFileName,
			size: this.asset.size,
			attributes: attributes
		};
	}
}
