import nodemailer, { TransportOptions, Transporter, SendMailOptions } from 'nodemailer';
import fs from 'fs';
import handlebars, { template } from 'handlebars';

const logger = require('pino')({name: 'djs:libs:dMailer'});

interface DMailerFromInterface {
  address: String,
  name: String
}

interface DMailerOptionsInterface {
  transporter?: any, // FIXME find the right interface pls
  from?: String | DMailerFromInterface,
  subject?: String,
  to?: String,
  template?: String,
  context?: any,
  text?: String,
  html?: String,
  viewsPath?: String,
  replyTo?: String
}

interface DMailerSendOptionsInterface {
  from?: String | DMailerFromInterface,
  to?: String,
  bcc?: String | String[], 
  subject?: String,
  text?: String,
  html?: String,
  template?: String,
  context?: any,
  replyTo?: String,
  attachments?: any[]
}

class DMailer {

  private options: DMailerOptionsInterface = {
    subject: 'no-title',
    to: 'someone',
    template: '',
    context: {},
    text: '',
    transporter: {
      sendmail: true,
      newline: 'unix',
      path: '/usr/sbin/sendmail'
    },
    from: {
      address: 'nobody@localhost',
      name: 'nobody'
    },
    viewsPath: 'app/views/mailers'
  };

  

  constructor(options?: DMailerOptionsInterface) {

    this.options = Object.assign(this.options, options || {});

    return this;

  }

  send(sendOptions: DMailerSendOptionsInterface = {}): Promise<any> {

    const transporter = nodemailer.createTransport(this.options.transporter);
    const to = sendOptions.to || this.options.to || '';
    const subject = sendOptions.subject || this.options.subject || '';
    const text = sendOptions.text || this.options.text || '';
    const attachments = sendOptions.attachments || [];
    let from = sendOptions.from || this.options.from || 'no-reply@localhost';
    
    // @ts-ignore
    if (typeof from === typeof {} && 'name' in from) {
      from = `${from.name} <${from.address}>`;
    }

    
    const sendMailOptions: any = { to, from, subject, text, bcc: sendOptions.bcc, attachments };

    if(this.options.replyTo) {
      sendMailOptions.replyTo = this.options.replyTo;
    }

    if (sendOptions.template || this.options.template) {

      const tpl = sendOptions.template || this.options.template || '';
      const context = sendOptions.context || this.options.context;

      let html = this.readMailerTemplates(tpl);
      const template = handlebars.compile(html.toString());
      sendMailOptions.html = template(context || {});

    }

    return new Promise((resolve, reject) => {

      transporter.sendMail(sendMailOptions, (err, info) => {
        logger.info(err ? err : info);
        if (err && Object.keys(err).length) {
          reject(err);
        } else {
          resolve(info);
        }
      });

    });

  }

  private readMailerTemplates(template: String) {
    const filePath = `${this.options.viewsPath}/${template}.handlebars`;
    if (fs.existsSync(filePath)) {
      return fs.readFileSync(filePath);
    } else {
      logger.error(`${filePath} doesn't exists`)
      throw new Error(`${filePath} doesn't exists`)
    }
  }

}

export default DMailer;