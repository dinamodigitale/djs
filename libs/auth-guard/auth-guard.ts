import { Request, Response, Express, Router } from 'express';

const logger = require('pino')({name: 'app:auth-guard'});

function tokenFromHeaderOrQuerystring(req: any) {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
}

function uidFromHeaderOrQuerystring(req: any) {
  if (req.headers['x-uid']) {
    return req.headers['x-uid'];
  } else if (req.query && req.query.uid) {
    return req.query.token;
  }
  return null;
}

export interface AuthControllerInterface {
  tokenExists: Function,
  addAuthorizationHeaders: Function,
}

export interface AuthGuardOptionsInterface {
  doNotRedirect?: boolean;
  onForbidden?: {
    status: number;
    type: "json";
    payload: {
      code: string
      error: string;
      details: string;
    }
  };
  authSuccesCb?: Function;
  authFailCb?: Function;
}

class AuthGuard {

  options: AuthGuardOptionsInterface = {
    doNotRedirect: false,
    onForbidden: {
      status: 401,
      type: "json",
      payload: {
        code: "unauthorizated",
        error: "unauthorizated",
        details: "Invalid authorization"
      }
    }
  }

  authController: AuthControllerInterface;

  constructor(authController: AuthControllerInterface, options: AuthGuardOptionsInterface = {}) {
    this.options = Object.assign(this.options, options);
    this.authController = authController;
  }
  
  use(app: Router ) {
    app.use((req: Request, res: Response, next: Function) => {
      process.env.DEBUG && logger.info("Token validation started for %s %s", req.method ,req.originalUrl);
      if(process.env.DJS_AUTH_GUARD_SKIP) {
        logger.info("DJS_AUTH_GUARD_SKIP is set, skipping token validation for %s %s", req.method ,req.originalUrl);
        return next();
      }

      this.authController.tokenExists(
        tokenFromHeaderOrQuerystring(req),
        uidFromHeaderOrQuerystring(req),
        req
      ).then((authRes: any) => {
        process.env.DEBUG && logger.info("Token validation success for %s %s", req.method ,req.originalUrl);
        //@ts-ignore
        req.user = authRes.user;
        this.authController.addAuthorizationHeaders(res, authRes.user, authRes.token);
        this.options.authSuccesCb ? this.options.authSuccesCb(authRes.user) : next();
      }, (err: any) => {
        process.env.DEBUG && logger.info("Token validation failed for %s %s", req.method ,req.originalUrl);
        if(this.options.doNotRedirect) {
          this.options.authFailCb ? this.options.authFailCb(err) : next();
        } else {
          process.env.DEBUG && logger.info("Sending %d response for %s %s",this.options.onForbidden ? this.options.onForbidden.status: 401, req.method ,req.originalUrl);
          res.status(this.options.onForbidden ? this.options.onForbidden.status : 401)[this.options.onForbidden ? this.options.onForbidden.type : 'json'](this.options.onForbidden ? this.options.onForbidden.payload: {
            error: "not authorizated",
            details: "Invalid authorization token"
          });
        }
      });

    });
  }
}

export default AuthGuard;