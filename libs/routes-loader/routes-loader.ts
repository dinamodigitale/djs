import fs from 'fs';
import express from 'express';
import deepmerge from 'deepmerge';
const walk = require('walkdir');

const logger = require('pino')({name: 'app:routes'});

class RoutesLoader {

  routesPath: string;

  constructor(routesPath: string) {
    this.routesPath = routesPath;
  }

  getRoutesBasePath(routePath: string) {
    return routePath.replace(this.routesPath, '');
  }

  getDepthLevel(routePath: string) {
    return this.getRoutesBasePath(routePath).split("/").filter(e => !!e).length - 1;
  }

  getDefaultRouteName(path: string) {
    let route = path.split("/");
    return route[route.length - 1];
  }

  getExpressRouter() {
    return express.Router({ mergeParams: true });
  }

  getRouteParent(routePath: string) {
    let tree = this.getRoutesBasePath(routePath).split("/").filter(e => !!e);
    return tree.length >= 2 ? tree[tree.length - 2] : null;
  }

  getRouteTreeLevels() {

    let levels: Array<any> = [];
    process.env.DEBUG && logger.info("Scanning %s", this.routesPath);
    let rootRoute = {
      path: this.routesPath,
      name: ''
    }
    Object.defineProperty(rootRoute, 'hasRequirableCandidate', {
      get: function () {
        return fs.readdirSync(this.path).findIndex(e => e.includes(".route")) >= 0;
      },
      enumerable: false
    });
    levels[0] = [rootRoute]

    walk.sync(this.routesPath, (path: string) => {

      if (!fs.lstatSync(path).isDirectory()) {
        return false;
      }

      let depth = this.getDepthLevel(path);

      const data = {
        path,
        defaultName: ''
      };

      Object.defineProperty(data, 'hasRequirableCandidate', {
        get: function () {
          return fs.readdirSync(this.path).findIndex(e => e.includes(".route")) >= 0;
        },
        enumerable: false
      });

      data.defaultName = this.getDefaultRouteName(path);
      process.env.DEBUG && logger.info("Loading routes for %s from %s", data.defaultName, data.path);

      if (!levels[depth]) {
        levels[depth] = [data];
      } else {
        levels[depth].push(data);
      }

    });

    return levels.reverse();

  }

  distributeRoutes(routeTreeLevels: Array<any>) {

    for (let depth = 1; depth <= routeTreeLevels.length - 1; depth++) {

      let level = routeTreeLevels[depth];

      for (let routeNumber in level) {

        let route = level[routeNumber];

        let appRouter = route.routerConfiguration.router();

        let routesToInclude = routeTreeLevels[depth - 1].filter((childRoute: any) => {
          return route.defaultName === childRoute.parent
        });

        for (let routeToInclude of routesToInclude) {
          appRouter.use(
            routeToInclude.routerConfiguration.basePath,
            routeToInclude.routerConfiguration.router()
          );

        }

        routeTreeLevels[depth][routeNumber].routerConfiguration.router = () => appRouter;

      }

    }

    return routeTreeLevels;

  }

  loadRoutes() {

    let routeTreeLevels = this.getRouteTreeLevels();

    for (let depth in routeTreeLevels) {

      let level = routeTreeLevels[depth];

      for (let routeNumber in level) {

        let route = level[routeNumber];

        if (route.hasRequirableCandidate) {

          let requirableRoute = `${route.path}/${fs.readdirSync(route.path).find(f => f.includes(".route"))}`;
          process.env.DEBUG && logger.info("requiring route %s", requirableRoute);
          try {
            const routerConfigurator = require(requirableRoute).default;
            route.routerConfiguration = routerConfigurator(this.getExpressRouter());
          } catch(e) {
            logger.error("Can't load the route", requirableRoute);
            logger.error(e);
            throw e;
          }
        }

        routeTreeLevels[depth][routeNumber] = deepmerge({
          parent: this.getRouteParent(route.path),
          routerConfiguration: {
            basePath: `/${route.defaultName}`,
            router: this.getExpressRouter
          }
        }, route);

      }

    }

    const distributedRoutes = this.distributeRoutes(routeTreeLevels);

    return distributedRoutes[distributedRoutes.length - 1];

  }

  loadRoutesIn(app: any) {
    for (let route of (this.loadRoutes() || [])) {
      const basePath = route.routerConfiguration.basePath;
      let path = basePath[0] === "/" ? basePath : basePath.substr(1);

      app.use(
        path,
        route.routerConfiguration.router()
      );

    }

  }

}

export default RoutesLoader;
