import fs from 'fs';

import { EventEmitter } from 'events';

import cors from 'cors';
import express from 'express';


import { MongooseLoader } from '..';
import { Configuration } from '..';
import { RoutesLoader } from '..';
import { Request, Response } from 'express';
import { Server } from 'http';

const Airbrake = require('airbrake');
const logger = require('pino')({name: 'dinamo-app'});
const EXIT_TIMEOUT = 10000;
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

interface DinamoAppOptionsInterface {
  port?: Number, // Default port
  express?: Boolean, // Do we need to load express and it's middile wares?
  dbms?: Boolean, // Load dbms
  appRoot?: String,
  errbitApiKey?: String,
  modelsAutoload?: Boolean,
  events?: {
    onOpen?: Function,
    onConnect?: Function,
    onDisconnect?: Function,
    /**
     * When express is ready
     */
    onListening?: Function,
    /**
     * When all the procedures are executed, this does not mean that
     * db is connected or express is listening
     */
    onBooted?: Function,
    onRoutesLoaded?: Function
  },
  avoidStaticUploads?: boolean
}

const defaults: DinamoAppOptionsInterface = {
  port: 3000, // Default port
  express: false, // Do we need to load express and it's middile wares?
  dbms: false, // Load dbms
  modelsAutoload: true,
  appRoot: '.',  
  events: {
    onListening: () => {},
    onConnect: () => logger.info('MongoDB connected'),
    onOpen: () => logger.info('MongoDB socket opened'),
    onDisconnect: () =>logger.warn('MongoDB disconnected'),
    onRoutesLoaded:() => logger.info("Routes Loaded"),
    onBooted: () => logger.info("App booted"),
  }
}

class DinamoApp extends EventEmitter {

  // Can be shared outside, example /djs/seeder.ts
  public dbms?: MongooseLoader;
  public express?: express.Express;
  private options: DinamoAppOptionsInterface = defaults;
  public listener: Server | undefined;
  constructor(options?: DinamoAppOptionsInterface) {

    super();

    this.options = Object.assign(defaults, options);
    process.env.DEBUG && logger.info("Starting app");

    process.env.DEBUG && logger.info("Setting app dir to %s", this.options.appRoot);

    if (this.options.dbms) {
      const mongoseConfig = process.env.DJS_MONGODB_CONNECTION ? {} : Configuration.get('mongoose-loader');
      const modelsPath = `${this.options.appRoot}/models`;
      this.dbms = new MongooseLoader(mongoseConfig);
      
      // process.on('SIGINT', () => {
      //   process.env.DEBUG && logger.info("Caught interrupt signal, disconnecting DB");
      //   this.disconnect()
      // });

      process.on('SIGTERM', () => {
        logger.info("Caught term signal, disconnecting DB");
        this.disconnect()
        setTimeout(() => {
          process.exit(0);
        }, EXIT_TIMEOUT)
      });
      
      if(this.options.modelsAutoload) {
        fs.readdirSync(modelsPath).filter((model: String) => {
          let ext = model.split('.').pop();
          return ext  === 'ts' || ext === 'js';
        }).forEach((model: String) => {
          process.env.DEBUG && logger.info(`Loading model %s`, model.split('.')[0]);
          require(`${modelsPath}/${model}`);
        });  
      }
      
      // @ts-ignore
      this.dbms.connect(this.options.events.onOpen, this.options.events.onConnect, this.options.events.onDisconnect);
    }

    if (this.options.express) {
      this.initExpress();
    }
    this.options.events && this.options.events.onBooted && this.options.events.onBooted(this.listener)
    logger.info("App ready")
    return this;
  }

  async disconnect() {
    if (this.dbms) {
     await this.dbms.disconnect();
    }
    if (this.express) {
      this.listener && this.listener.close()
    }
  }


  private initExpress() {

    process.env.DEBUG && logger.info("Initializing express");

    this.express = express();

    // Set express env
    this.express.set('env', process.env.NODE_ENV);

    // Setup cors headers
    this.express.use(cors({
      credentials: true,
      origin: true,
      exposedHeaders: [
        'Content-Type',
        'Authorization',
        'X-UID',
        'X-Api-Version',
        'X-Pagination-Has-Next-Page',
        'X-Pagination-Has-Prev-Page',
        'X-Pagination-Limit',
        'X-Pagination-Page',
        'X-Pagination-Total-Pages',
        'X-Pagination-Total-Records',
        'Content-Disposition'
      ]
    }));

    let airbrake;

    if ('development' !== process.env.NODE_ENV && this.options.errbitApiKey) {

      airbrake = Airbrake.createClient(
        '1', // Project ID
        this.options.errbitApiKey, // Project key,
        process.env.NODE_ENV
      );

      airbrake.protocol = 'http';
      airbrake.serviceHost = 'errbit.dinamocloud.com';
      airbrake.handleExceptions();

    }

    
    if(!this.options.avoidStaticUploads) {
      this.express.use('/uploads', express.static('uploads'));
    }
    
    this.express.get('/status', (_, res: Response) => {
      res.status(200).json({
        uptime: process.uptime(),
        version: process.version,
        versions: process.versions,
        title: process.title,
        pid: process.pid,
        ppid: process.ppid,
        release: process.release,
        platform: process.platform,
        cpuUsage: process.cpuUsage(),
        memoryUsage: process.memoryUsage(),
        arch: process.arch,
        config: process.config,
        maxListeners: process.getMaxListeners(),
        env: process.env.NODE_ENV !== 'development' ? undefined : process.env,
      });
    });

    // Parse request as application/json, no need to body-parser module
    this.express.use(express.json({limit: process.env.EXPRESS_MAX_UPLOAD_SIZE || '256mb'}));

    this.express.use((req: Request, res: Response, next: Function) => {
      const startTime = +new Date();
      logger.info('Request started %s %s', req.method, req.originalUrl);
      res.on('finish',() =>{
        const timeDiff = (+new Date() - startTime)/1000;
        // strip the ms
        logger.info('Reponse %s %s %s in %ss', res.statusCode, req.method, req.originalUrl, timeDiff.toFixed(3));
      })
      next()
    });

    this.express.use((req: any, res: any, next: any) => {
      res.set('X-Powered-By', 'DinamoDigitale');
      next();
    });

    const routesLoader = new RoutesLoader(`${this.options.appRoot}/routes`);

    routesLoader.loadRoutesIn(this.express);
    this.options.events?.onRoutesLoaded && this.options.events?.onRoutesLoaded(this.express)
    
    this.listener = this.express.listen(process.env.PORT || this.options.port, () => {
      logger.info(`App is listening on port %o`, process.env.PORT || this.options.port);
      this.options.events && this.options.events.onListening && this.options.events.onListening();
    });


    if ('development' !== process.env.NODE_ENV && this.options.errbitApiKey) {
      this.express.use(airbrake.expressHandler());
    }

    this.listener.on('error', (err: any) => {
      logger.error(`ERROR, Can't listen to port ${process.env.PORT || this.options.port}, the port is in use or the access is denied`);
      throw new Error(err);
    });

    this.listener.on('close', () => {
      process.env.DEBUG && logger.info(`Express socket closed on port ${process.env.PORT || this.options.port}`);
    });

    // process.on('SIGINT', () => {
    //   process.env.DEBUG && logger.info(`Caught interrupt signal, closing express socket on port ${process.env.PORT || this.options.port}`);
    //   listener.close((err) => process.exit(0));
      
    // });
    
    process.on('SIGTERM', () => {
      process.env.DEBUG && logger.info(`Caught term signal, closing express socket on port ${process.env.PORT || this.options.port}`);
      this.listener && this.listener.close((err) => {
        process.env.DEBUG && logger.info('Closing listener socket')
        if(err) {
          logger.error(err)
        }
      });
      setTimeout(() => {
        process.exit(0);
      }, EXIT_TIMEOUT)
    });
  };

}

export { DinamoApp };