class DinamoAppDbmsInterface {

  initializeConnection: any;
  disconnectDb: any;

  constructor() {

    if (new.target === DinamoAppDbmsInterface) {
      throw new TypeError("Cannot construct Interface instances directly");
    }

    if (this.initializeConnection === undefined) {
      throw new TypeError("Must override initializeConnection");
    }

    if (this.disconnectDb === undefined) {
      throw new TypeError("Must override disconnectDb");
    }

  }

}

export default DinamoAppDbmsInterface;