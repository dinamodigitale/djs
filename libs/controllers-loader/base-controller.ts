import { Document, Model } from "mongoose";
import { mongoose } from "../mongoose-loader";
import { NextFunction, Request, Response, Router } from 'express';

interface MongooseQueryInterface {
  [x: string]: any;
}

interface QueryOptionsInterface {
  index?(req: Request): MongooseQueryInterface;
  show?(req: Request): MongooseQueryInterface;
}

export type PolicyCallbackInterface  = (req: Request, res: Response, next: Function) => void;
export type MiddlewareFunctionInterface = (req: Request & any, res: Response, next: NextFunction) => Promise<void> 

export interface PoliciesOptionsInterface {
  create?: PolicyCallbackInterface | PolicyCallbackInterface[]
  index?: PolicyCallbackInterface | PolicyCallbackInterface[]
  show?: PolicyCallbackInterface | PolicyCallbackInterface[]
  update?: PolicyCallbackInterface | PolicyCallbackInterface[]
  delete?: PolicyCallbackInterface | PolicyCallbackInterface[]
}

export interface PopulateOptionsInterface {
  index?: string | Object | Array<string | Object>;
  show?: string | Object | Array<string | Object>;
  update?: string | Object | Array<string | Object>;
}

export interface ResourcifyOptionsInterface {
  policies?: PolicyCallbackInterface | PoliciesOptionsInterface;
  readOnly?: Boolean;
  declareRouteFor?: Array<String>;
  populate?: PopulateOptionsInterface;
  query?: QueryOptionsInterface;
  sort?: any;
  select?: any;
  pagination?: boolean;
  paginationUseV1?: boolean;
  lean?: boolean;
  transform?: (doc: any, req?: Request) => any 
  middleware?: {
    create?: MiddlewareFunctionInterface | MiddlewareFunctionInterface[]
    index?: MiddlewareFunctionInterface | MiddlewareFunctionInterface[]
    show?: MiddlewareFunctionInterface | MiddlewareFunctionInterface[]
    update?: MiddlewareFunctionInterface | MiddlewareFunctionInterface[]
    delete?: MiddlewareFunctionInterface | MiddlewareFunctionInterface[]
  } 
}

export interface ResourcifyIndexOptionsInterface extends ResourcifyOptionsInterface {
  populate?: PopulateOptionsInterface;
  query?: QueryOptionsInterface;
  sort?: any;
  select?: any;
  pagination?: boolean;
  paginationUseV1?: boolean;
  lean?: boolean;
  transform?: (doc: any, req?: Request) => any 
}

const fs = require('fs-extra');
const logger = require('pino')({name: 'app:controllers'});

const getLogger = (name: any, opts?: any) => logger //.child({controller: name, ...opts});

export const index = (model: any, name: String, options: ResourcifyIndexOptionsInterface) => {
  return async (req: any, res: any) => {
    
    const logger = getLogger(name);

    logger.info("index() function from BaseController has been called", req.params, req.query, req.body);

    const q =  options.query && options.query.index ? await options.query.index(req) : {};
    
    Object.assign(q, req.body.query);
    
    let find = model.find(q);

    if (req.body.populate) {
      if (Array.isArray(req.body.populate)) {
        for (let p of req.body.populate) {
          find = find.populate(p);
        }
      } else {
        find = find.populate(req.body.populate);
      }
    } else if (options.populate && options.populate.index) {
      if (Array.isArray(options.populate.index)) {
        for (let p of options.populate.index) {
          find = find.populate(p);
        }
      } else {
        find = find.populate(options.populate.index);
      }
    }

    
    if(req.body.sort) {
      find = find.sort(req.body.sort);
    } else if(options.sort) {
      find = find.sort(options.sort);
    }
   
    if(req.body.select) {
      find = find.select(req.body.select);
    } else if(options.select) {
      find = find.select(options.select);
    }
    
    if(req.body.limit) {
      find = find.limit(parseInt(req.body.limit))
    }
    
    if(req.body.offset) {
      find = find.offset(parseInt(req.body.offset))
    }

    if(options.pagination && req.params['disable-pagination'] !== 'true') {
      if(options.paginationUseV1) {
        find = find.paginate(req, res);
      } else {
        find = find.pagination(req.body.limit || req.query.limit, req.body.page || req.query.page, options.lean);
      }
    }
    else if (options.lean) {
      find = find.lean()
    }

    try {
      const results = await find
      let data: any = {}
      if(options.transform) {
        if (options.pagination) {
          data = {...results}
          //@ts-ignore
          data.data = data.data.map((r: any)=> options.transform(r, req))
        }
        else {
          //@ts-ignore
          data = results.map((r: any)=> options.transform(r, req))
        }
      } 
      else {
        data = results
      }
        
      res.status(200).json(data)
    } catch (err) { 
      logger.error(err);  
      res
        .status(err.name && err.name === 'ValidationError' ? 422 : 500)
        .send({
          message: err.message,
          name: err.name,
          validation: err.name === 'ValidationError' ? err : undefined,
          fileName:  process.env.NODE_ENV === 'production' ? undefined : __filename,
          stack: process.env.NODE_ENV === 'production' ? undefined : err.stack.split("\n"),
        });
    }
  };
}


export const show = (model: any, name: String, populate?: PopulateOptionsInterface, query?: QueryOptionsInterface, options?: Partial<ResourcifyOptionsInterface>) => {

  return async (req: any, res: any) => {

    const logger = getLogger(name);


    logger.info("show() function from BaseController has been called", req.params, req.query);

    const q = {
      _id: req.params.id
    };
    
    // FIX ME, is this a bug? why do we use query.index instead of query.show
    if (query && query.index) {
      Object.assign(q, await query.index(req));
    }

    let find = model.findOne(q);

    if (populate && populate.show) {

      if (Array.isArray(populate.show)) {
        for (let p of populate.show) {
          find = find.populate(p);
        }
      } else {
        find = find.populate(populate.show);
      }

    }
    try {
      let item = await find;
      if(item && options?.transform) {
        item = options.transform(item, req);
      }
      res
        .status(item ? 200 : 404)
        .send(item || { error: 'not-found'});
    } catch (err) {
      logger.error(err);
      res
        .status(err.name && (err.name === 'ValidationError') ? 422 : 500)
        .send({
          message: err.message,
          name: err.name,
          validation: err.name === 'ValidationError' ? err : undefined,
          fileName:  process.env.NODE_ENV === 'production' ? undefined : __filename,
          stack: process.env.NODE_ENV === 'production' ? undefined : err.stack.split("\n"),
        });
    }
    
  }

};

export const create = (model: any, name: any) => {

  return async (req: any, res: any) => {

    const logger = getLogger(name);


    logger.info("create() function from BaseController has been called", req.params, req.query);
    
    try {
      let item = await model.create(req.body);
      res.status(201).json(item);
    } catch (err) {
      logger.error(err);
      res
        .status(err.name && (err.name === 'ValidationError') ? 422 : 500)
        .send({
          message: err.message,
          name: err.name,
          validation: err.name === 'ValidationError' ? err : undefined,
          fileName:  process.env.NODE_ENV === 'production' ? undefined : __filename,
          stack: process.env.NODE_ENV === 'production' ? undefined : err.stack.split("\n"),
        });
    }
  }
};

export const update = (model: any, name: any, populate?: PopulateOptionsInterface) => {

  return async (req: any, res: any) => {

    const logger = getLogger(name);


    logger.info("update() function from BaseController has been called", req.params, req.query);

    findByIdAndSave(model, req.params.id, req.body, function(err: Error, doc: Document) {
      if(err) {
        logger.warn("An error occured while saving the document %o", err.message);
        console.error(err)

        return res.status(err.name && err.name === 'ValidationError' ? 422 : 500).json(err);
      }

      let find = model.findById(req.params.id)
     
      if (populate && populate.update) {
          if (Array.isArray(populate.update)) {
          for (let p of populate.update) {
            find = find.populate(p);
          }
        } else {
          find = find.populate(populate.update);
        }
        }
  
      find.then((item: any) => {
        res.status(item ? 200 : 404).send(item || {
          error: 'not-found'
        });
      }, (err: any) => {
        logger.error(err);
        res
          .status(err.name && (err.name === 'ValidationError') ? 422 : 500)
          .send({
            message: err.message,
          name: err.name,
          validation: err.name === 'ValidationError' ? err : undefined,
          fileName:  process.env.NODE_ENV === 'production' ? undefined : __filename,
          stack: process.env.NODE_ENV === 'production' ? undefined : err.stack.split("\n"),
          });
      });
    })
  }
};

export const remove = (model: any, name: any) => {

  return async (req: any, res: any) => {

    const logger = getLogger(name);


    logger.info("delete() function from BaseController has been called", req.params, req.query);
    try {
      const deleted = await model.findOneAndRemove({_id: req.params.id});
      res.status(deleted ? 204 : 404).end()
    } catch (err) {
      logger.error(err);
      res
        .status(err.name && (err.name === 'ValidationError') ? 422 : 500)
        .send({
          message: err.message,
          name: err.name,
          validation: err.name === 'ValidationError' ? err : undefined,
          fileName:  process.env.NODE_ENV === 'production' ? undefined : __filename,
          stack: process.env.NODE_ENV === 'production' ? undefined : err.stack.split("\n"),
        });
    }
  }
};


export function findByIdAndSave( model: any, id: any, data: any, next: any ){

  model.findById( id, function( err: any, doc:any) {
    if( err ){
      next( err, null );
    } else {
      if(! doc ){
        logger.error("DOCUMENT NOT FOUND",model.collection.collectionName, id);
        next( new Error("Object to save not found"), null );
      } else {
        // There must be a better way of doing this
        for( var k in data ){
          doc[k] = data[k];
        }
        doc.save( next );
      }
    }
  });
}

class BaseController {

  static index: any = undefined;
  static show: any = undefined;
  static create: any = undefined;
  static update: any = undefined;
  static delete: any = undefined;

  static fileNamePathComposer(filename: Boolean | String = false, entity = false) {
    var now = new Date();
    return [
      'uploads',
      entity,
      now.getFullYear(),
      now.getMonth() + 1,
      now.getDate(),
      filename
    ].filter(f => f !== false).join('/');
  }

  //TODO Remove from this class
  static moveSync(file: any, entity = false) {

    const newDir = BaseController.fileNamePathComposer(false, entity);
    fs.ensureDirSync(newDir);
    const newPath = BaseController.fileNamePathComposer(`${+new Date()}-${file.originalname.replace(/\s+/g, '')}`, entity);
    fs.renameSync(file.path, newPath);
    return {
      path: newPath,
      mimetype: file.mimetype,
      originalname: file.originalname,
      size: file.size
    };
  }

  //TODO Remove from this class
  static isValidId(id: string): boolean {
    return mongoose.Types.ObjectId.isValid(id);
  }

  //TODO Remove from this class
  static areValidIds(ids: Array<string>): boolean {
    const c = ids.filter(id => mongoose.Types.ObjectId.isValid(id));
    return c.length === ids.length;
  }

  static resourcify(router: any, model: Model<any>, options: ResourcifyOptionsInterface = {}) {
    
    if (!this.index) {
      this.index = index(model, this.name, options);
    }

    if (!this.show) {
      this.show = show(model, this.name, options.populate, options.query, options);
    }

    if (!this.create) {
      this.create = create(model, this.name);
    }

    if (!this.update) {
      this.update = update(model, this.name, options.populate);
    }

    if (!this.delete) {
      this.delete = remove(model, this.name);
    }

    const policies: PoliciesOptionsInterface = {
      index: (req: Request, res: Response, next: Function) => next(),
      show: (req: Request, res: Response, next: Function) => next(),
      create: (req: Request, res: Response, next: Function) => next(),
      update: (req: Request, res: Response, next: Function) => next(),
      delete: (req: Request, res: Response, next: Function) => next()
    };

    if (options.policies) {
      if (typeof options.policies === 'function') {
        // @ts-ignore
        Object.keys(policies).forEach(key => policies[key] = options.policies);
      } else {
        Object.assign(policies, options.policies);
      }
    }

    const InjectResource = async (req: any,_res: any,next: any) => {
      req.resource = await model.findById(req.params.id)
      next()
    }
    
    const LoggerMiddleware = (req: Request, res: Response, next: Function) => {
      const logger = getLogger(`BaseController by ${this.name}`, {
        params: req.params, query:req.query});
      process.env.DEBUG && logger.info('BaseController processing request %s %s', req.method, req.originalUrl);
      next();
    }

    if (BaseController.declarableRoute('index', options)) {
      router.get('/', LoggerMiddleware, options.middleware?.index || [], policies.index, this.index);
    }

    if (BaseController.declarableRoute('resource', options)) {
      router.post('/resource', LoggerMiddleware, options.middleware?.index || [], policies.index, this.index);
    }

    if (BaseController.declarableRoute('show', options)) {
      router.get('/:id', LoggerMiddleware, InjectResource,options.middleware?.show || [], policies.show, this.show);
    }

    if (!options || (options && !options.readOnly)) {
      if (BaseController.declarableRoute('create', options)) {
        router.post('/', LoggerMiddleware, options.middleware?.create || [], policies.create, this.create);
      }
      if (BaseController.declarableRoute('update', options)) {
        router.patch('/:id', LoggerMiddleware, InjectResource, options.middleware?.update || [], policies.update, this.update);
      }
      if (BaseController.declarableRoute('delete', options)) {
        router.delete('/:id', LoggerMiddleware, InjectResource, options.middleware?.delete || [], policies.delete, this.delete);
      }
    }

  }

  private static declarableRoute(type: String, options: ResourcifyOptionsInterface) {
    return (
      !options ||
      !options.declareRouteFor ||
      options.declareRouteFor.indexOf(type) >= 0
    );
  }

}

export default BaseController;