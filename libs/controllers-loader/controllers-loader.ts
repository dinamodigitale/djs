import fs, { PathLike } from 'fs';

const logger = require('pino')({name: 'app:libs:controllers-loader'});

class ControllersLoader {

  private controllerPath: PathLike = '';
  private controllers: Array<any> = [];

  constructor(controllerPath: PathLike) {

    this.controllerPath = controllerPath;

    this.loadControllers();
    // this.addControllersToGlobal();

  }

  private loadControllers() {

    logger.info("Scannig %s", this.controllerPath);
  
    const filesList = fs.readdirSync(this.controllerPath);
  
    for (let file of filesList) {
  
      let controller = require(`${this.controllerPath}/${file}`);
      this.controllers.push(controller);
  
    }
  
  }
  
  // private addControllersToGlobal() {
  //   for (let controller of this.controllers) {
  //     logger.info("Adding", controller.name, 'to global scope');
  //     global[controller.name] = controller
  //   }
  // }

}

export default ControllersLoader;
