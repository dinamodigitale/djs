export interface I18nInterface {
  [x: string]: string | boolean,
  _i18n: boolean,
}

export interface I18nOptionsInterface {
  default?: string;
  fallback?: {
    [x: string]: string[]
  }
}

const logger = require('pino')({name: 'app:i18n'});

class I18n {

  locales: Array<string> = [];
  options: I18nOptionsInterface = {
    fallback: {}
  };

  constructor(locales?: Array<string>, options?: I18nOptionsInterface) {
    this.locales = locales?.length ? locales : ['en'];
    this.options = { ...this.options, ...options }
    if (!this.options.default) {
      this.options.default = this.locales[0];
    }
    process.env.DEBUG && logger.info("Init default locale: %s, availabe locales %o, options %o", this.options.default, this.locales, this.options)
  }

  mongooseModel(locales?: Array<string> | null, mongooseAdditionalConfigurations?: Object): {
    [x: string]: any,
    type: I18nInterface
  } {
    if (!locales) {
      locales = (this.locales as Array<string>) || [];
    }
    if (!mongooseAdditionalConfigurations) {
      mongooseAdditionalConfigurations = {};
    }
    const mongooseModel: any = {};
    locales.forEach((locale: string) => {
      mongooseModel[(locale as string)] = {
        type: String,
        default: ''
      };
    });
    return Object.assign({
      default: {}, // uncomment this to enable the validation
      set: (obj: any) => {
        locales?.forEach((locale: string) => {
          obj[locale] = obj[locale] || '';
        });
        return { ...obj, _i18n: true }
      }
    }, mongooseAdditionalConfigurations, {
      type: mongooseModel as I18nInterface
    });
  }

  serveLocale(jsonObj: any, locale: any, options: any = null) {
    if (!jsonObj) {
      return jsonObj
    }
    Object.keys(jsonObj).forEach(key => {

      if (jsonObj[key] instanceof Object) {

        if (jsonObj[key]._i18n === false && (options || { exclude: [] }).exclude.indexOf(key) < 0) {
          jsonObj[key] = jsonObj[key][locale];
        } else if (jsonObj[key]._i18n === true && (options || { exclude: [] }).exclude.indexOf(key) < 0) {
          jsonObj[key] = this.fallback(jsonObj[key], locale);
        } else {
          this.serveLocale(jsonObj[key], locale, options);
        }

      }

    });

    return jsonObj;

  }

  fallback(jsonObj: any, locale: any): string | null {
    if ('string' === typeof jsonObj[locale] && jsonObj[locale].trim()) {
      return jsonObj[locale];
    }
    if (this.options.fallback && this.options.fallback[locale]) {
      for (let l of this.options.fallback[locale]) {
        if ('string' === typeof jsonObj[l] && jsonObj[l].trim()) {
          return jsonObj[l]
        }
      }
    }
    return undefined === jsonObj[locale] ? null : jsonObj[locale];
  }

  i18nRoute(req: any, res: any, next: any) {
    res.translate = (jsonObject: any, options: any = null) => {
      jsonObject = JSON.parse(JSON.stringify(jsonObject));
      return res.json(this.serveLocale(jsonObject, req.params.locale, options));
    };
    next();
  }

  static slugify(text: string) {
    const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;’'
    const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz-------'
    const p = new RegExp(a.split('').join('|'), 'g')
    if (!text) {
      return ''
    }
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[^\w\d_\u0400-\u9FBF\s-]+/g, '') // Remove all non-word characters
      .replace(/\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  }

  i18nSlugify(text: string | I18nInterface): I18nInterface {
    const slug: I18nInterface = {
      _i18n: true
    };
    if (typeof text === typeof '') {
      this.locales.forEach(locale => {
        Object.assign(slug, {
          [locale]: I18n.slugify((text as string))
        });
      });
    } else {
      Object.keys(text).forEach(locale => {
        if (locale !== '_i18n') {
          Object.assign(slug, {
            [locale]: I18n.slugify(((text as I18nInterface)[locale] as string))
          });
        }
      });
    }

    return slug;
  }

  stringToI18nObject(str: String) {
    if (!str) { return null };
    let out: any = { _i18n: true };
    this.locales.forEach((locale: string) => {
      out[locale] = str
    });
    return out;
  }

}

export default I18n;
