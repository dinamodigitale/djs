import 'module-alias/register';
import { ArgumentParser } from 'argparse';
import * as path from 'path';
import fs from 'fs';

import { SeederGenerator, Seeder, DinamoApp } from '..';

const logger = require('pino')({name: 'app:seed'});

const appRoot = path.resolve(`${(__dirname)}/../../app`);

const seedsPath = `${appRoot}/seeds`;

const parser = new ArgumentParser({
  version: '1.0.0',
  addHelp: true,
  description: 'Argparse examples: sub-commands',
});

const subparsers = parser.addSubparsers({
  title: 'subcommands',
  dest: "action"
});

const executor = subparsers.addParser('exec', {
  aliases: ['e'],
  addHelp: true
});

const generator = subparsers.addParser('generate', {
  aliases: ['g'],
  addHelp: true
});

executor.addArgument(['--all'], {
  action: 'storeTrue',
  help: 'launch all the seeds - it will use the timestamp in front of the file for the execution order'
});

executor.addArgument(['--only'], {
  type: 'string',
  help: 'Lunch a single seed only, specify the exact filename'
});

executor.addArgument(['--from'], {
  type: 'int',
  help: 'Lunch al seed thar are greater or equal to the specified timestap'
});

generator.addArgument(['--name'], {
  help: 'the name of seed - it will be used as class name',
  required: true
});

generator.addArgument(['--model'], {
  help: 'the model to refers'
});

const args = parser.parseArgs();


if (!process.env.NODE_ENV && args.action !== 'generate' && args.action !== 'g') {
  console.error(`
    Must specify an NODE_ENV. You can choose between development or production

    NODE_ENV=development node seeds.js
    NODE_ENV=production node seeds.js

  `);
  process.exit();
}

if (args.action === 'generate' || args.action === 'g') {

  const seedGenerator = new SeederGenerator(appRoot + '/seeds');

  try {
    seedGenerator.generate(args.name, args.model ? args.model : undefined);
  } catch (e) {
    process.exit();
    // exit(e, 'generate error');
  }

  process.exit();
  // exit(null, `${args.name} generated with success`);

} else {

  logger.info(`Start Seeding for ${process.env.NODE_ENV}`);

  const seeds: Array<Seeder> = [];
  if(args.only) {
    seeds.push(require(`${seedsPath}/${args.only}`).default);
  } else {
    logger.info(`Scannig ${seedsPath}`);
    fs.readdirSync(seedsPath).forEach((file: any) => {
      if (args.from) {
        if (args.from && args.from <= parseInt(file.split('_')[0])) {
          seeds.push(require(`${seedsPath}/${file}`).default);
        }
      } else {
        seeds.push(require(`${seedsPath}/${file}`).default);
      }
    });
  }

  const startSeeding = async () => {

    for (const Seed of seeds) {
      // @ts-ignore
      const seeder = new Seed();
      logger.info("Running seed %s", seeder.constructor.name);
      const fn = process.env.NODE_ENV === 'development' ? 'developmentSeed' : 'productionSeed';
      try {
        await seeder[fn]();
      } catch(e) {
        logger.info('An error occured while running %o',fn);
        logger.error(e);
        process.exit(1);
      }
    }
    return;
  }

  const app = new DinamoApp({
    dbms: true,
    express: false,
    appRoot: appRoot,
    events: {
      onConnect: () => {
        startSeeding().then(res => {
          exit(null);
        });
      }
    }
  });

  function exit(err: Error | null, message?: string) {
    if (err) {
      logger.error(err);
    }
    if (app && app.dbms) {
      app.dbms.disconnect();
      logger.info('Done');
    }
  }

}